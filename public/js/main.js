//Send messages via ajax
$("#messageBot").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    var message = $('#message').val();

    var typing = $('.typing');

    var chat = $('.chat');

    var username = $('#username').val();

    if(!message){
        alert('Please enter a command.');
    }
    else {
        chat.append('<div class="user-message"><h5>'+ username +'</h5><p>' + message + '</p></div>');
        typing.show();
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                typing.hide();
                if(!data){
                    //Clear message box
                    $('#message').val(' ');

                    //Return default message if one of the commands aren't submitted
                    chat.append('<div class="bot-message"><h5>GlobeeBot</h5><p>Please send one of the commands to utilize GlobeeBot.</p></div>');
                } else{
                    //Clear message box
                    $('#message').val(' ');

                    //Return message from server
                    chat.append('<div class="bot-message"><h5>GlobeeBot</h5><p>' + data['text'] + '</p></div>');
                }
            }
        });
    }
});

//Save settings via ajax
$("#save-settings").submit(function(e) {

    e.preventDefault(); // avoid to execute the actual submit of the form.

    var form = $(this);
    var url = form.attr('action');

    var currency = $('#currencies').val();

    var progress = $('.settings-progress');

    var notices = $('.notice-box');

    if(!currency){
        alert('Please select a currency.');
    }
    else {
        progress.show();
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                progress.hide();
                if(!data){
                    //Return default message if nothing returned
                    notices.html('<div class="alert alert-secondary" role="alert">No settings have been changed.</div>');
                } else{
                    //Send success/error messages to view
                    if(data.success){
                        notices.html('<div class="alert alert-success" role="alert">' + data.success + '</div>');
                    }
                    else{
                        notices.html('<div class="alert alert-danger" role="alert">' + data.error + '</div>');
                    }
                }
            }
        });
    }
});
