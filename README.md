**Config, Helper, Controllers and Ajax**

---

## Config

1. The app uses global variables which can be found in the `.env.example` file. So please rename it to `.env` and add your db details. (Currently set to my testing db)
2. For the database tables run in terminal `php artisan migrat`
3. For the initial user run in terminal `php artisan db:seed --class=UsersTableSeeder
`

##### Login

For logging in as the test user:

```angular2
Email: sheep@paybee.co
Password: P@ssw0rd!
```


---

## Helper

The app uses a Helper found in `app/Http/Helpers` named `BotHelper.php`. It has already been loaded in `composer.json` file.

If you add a new helper function, just run `composer dump-autoload` in terminal for the app.

---


## Controllers

There are only two controllers being used:

- HomeController: For initial login and setting up Telegram linking
- BotController: For messages sent between user and bot and for bot settings to be saved

---

## Ajax

There are only two ajax requests to the app found in `public/js/main.js` for these routes:

- /message/bot : messages sent via chat
- /bot-config : settings saved via settings page

---

