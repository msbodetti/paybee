<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Sheep',
            'email' => 'sheep@paybee.co',
            'password' => bcrypt('P@ssw0rd!'),
        ]);
    }
}
