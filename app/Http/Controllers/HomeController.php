<?php

namespace App\Http\Controllers;

use App\TelegramUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //logged in user id
        $paybeeId = Auth::user()->id;

        //Generate unique code for Telegram
        $uniqueCode = bin2hex(random_bytes(8));

        //Telegram user model
        $telegramUser = new TelegramUser();

        //Get Telegram user by user id
        $authTelegram = $telegramUser->where('paybee_id', '=', $paybeeId)->first();

        if($authTelegram)
        {
            //Get Telegram user unique code from model
            $uniqueCode = $telegramUser->where('paybee_id', '=', $paybeeId)->first()->code;

            //Check if logged in user has already started chat with bot
            if($uniqueCode) {
                $botUpdates = getUpdates();

                $offset = 0;
                foreach ($botUpdates as $update)
                {
                    $offset = max($offset, $update['update_id']);

                    $message = $update['message'];

                    $chatId = $message['chat']['id'];

                    $text = $message['text'];

                    if (!str_startswith($text, '/start')) {
                        continue;
                    }

                    $parts = explode(' ' , $text);

                    if (count($parts) === 2) { // ['/start', '<uniqueCode>']
                        $uniqueCode = $parts[1];

                        //Save id to telegram user table
                        TelegramUser::where('paybee_id', '=', $paybeeId)->update(array('user_id' => $chatId));
                        session()->put('user_id', $chatId);
                    }

                    if($uniqueCode !== null) {
                        session()->put('botchat', true);
                    }

                    $offset++;
                }
            }
        } else {
            //Save logged in user info to telegram table
            $telegramUser->paybee_id = $paybeeId;
            $telegramUser->code = $uniqueCode;
            $telegramUser->user_id = 0;
            $telegramUser->username = Auth::user()->name;
            $telegramUser->save();
        }


        $url = sprintf('https://telegram.me/%s?start=%s', env('BOT_NAME'), $uniqueCode);
        return view('home', compact('url'));
    }
}
