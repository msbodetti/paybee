<?php

namespace App\Http\Controllers;

use App\BotConfig;
use App\TelegramUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BotController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Return messages to user via coindesk and telegram api
     *
     * @return json
     */
    public function message(Request $request)
    {
        //logged in user id
        $paybeeId = Auth::user()->id;

        $message = $request->input('message');

        $returnMessage = null;

        //Models
        $botConfig = new BotConfig();

        $chatId = session()->get('user_id');

        if($message) {
            //Get current BTC rate command
            if(str_startswith($message, '/getbtcquivalent')) {
                $parts = explode(' ' , $message);

                //Get value
                $value = $parts[1];

                //Get currency of current logged in user
                $myCurrency = $botConfig->where(['paybee_id' => $paybeeId, 'config_key' => 'user_currency'])->first();

                //Get currency
                if(!empty($parts[2])){
                    $currency = $parts[2];
                } else{
                    $currency = $myCurrency->config_value;
                }

                //Send request to coindesk api
                $getRate= getBtc($currency);

                if($getRate){
                    $rate = $getRate['bpi'][$currency]['rate_float'];
                    $converted = number_format((float)$value/$rate, 2, '.', '');
                    $currentRate = number_format((float)$rate, 2, '.', '');
                    $rateMessage = $value . ' ' . $currency . ' is ' . $converted . ' BTC ('. $currentRate . ' ' . $currency . ' - 1 BTC)';

                    //Send message via telegram api
                    $returnMessage = sendMessage($chatId, $rateMessage);
                }
            }

            //Get current chat user id command
            if(str_startswith($message, '/getuserid')) {
                //Send message via telegram api
                $returnMessage = sendMessage($chatId, $chatId);
            }
        }

        return $returnMessage;
    }

    public function settings()
    {
        //logged in user id
        $paybeeId = Auth::user()->id;

        //Get currencies from coindesk
        $currencies = getCurrencies();

        //Models
        $botConfig = new BotConfig();
        $telegramUser = new TelegramUser();

        //Get currency of current logged in user
        $myCurrency = $botConfig->where(['paybee_id' => $paybeeId, 'config_key' => 'user_currency'])->get();

        //Get telegram user id of current logged in user
        $telegramUserId = TelegramUser::where('paybee_id', '=', $paybeeId)->first()->user_id;

        //Get Telegram user unique code from model
        $uniqueCode = $telegramUser->where('paybee_id', '=', $paybeeId)->first()->code;

        $url = null;

        if(!$myCurrency){
            $myCurrency = null;
        }
        else{
            $myCurrency = $myCurrency->config_value;
        }

        if(!$telegramUserId){
            $telegramUserId = null;

            //Let's generate telegram account linking
            $url = sprintf('https://telegram.me/%s?start=%s', env('BOT_NAME'), $uniqueCode);
        }

        return view('home', compact('currencies', 'myCurrency', 'telegramUserId', 'url'));
    }

    public function saveSettings(Request $request)
    {
        //logged in user id
        $paybeeId = Auth::user()->id;

        //Get currency from select
        $currency = $request->input('currencies');

        //Models
        $botConfig = new BotConfig();

        //Create or add new currency to model
        $currencyInsertOrUpdate = $botConfig::where('paybee_id', '=', $paybeeId)->firstOrNew(array('config_key' => 'user_currency'));
        $currencyInsertOrUpdate->config_value = $currency;
        $currencyInsertOrUpdate->paybee_id = $paybeeId;
        $saved = $currencyInsertOrUpdate->save();

        $message = null;

        //Send success/error message to view
        if($saved){
            $message['success'] = 'Settings have been saved';
        } else{
            $message['error'] = 'Something went wrong. Please try again later.';
        }

        return $message;
    }
}
