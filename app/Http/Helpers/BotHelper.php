<?php

if (!function_exists('telegramRequest')) {
    /**
     * Making request with telegram api
     *
     * @param  string $url URL
     * @return json
     */
    function telegramRequest($url)
    {
        $result = json_decode(file_get_contents($url), true);
        if ($result['ok'] !== true) {
            throw new RuntimeException('Request failed. Telegram API reported: '.$result['description']);
        }
        return $result['result'];
    }
}

if (!function_exists('getUpdates')) {
    /**
     * Get updates from telegram API for bot
     *
     * @return json
     */

    function getUpdates()
    {
        $query = http_build_query(['offset' => 0, 'timeout' => 20, 'allowed_updates' => ['message']]);
        $url = env('BOT_API_URL') . env('BOT_TOKEN') . '/getUpdates?'. $query;

        return telegramRequest($url);
    }

}

if (!function_exists('str_startswith')) {
    /**
     * Check if message starts with $val
     *
     * @return boolean
     */

    function str_startswith($str, $val)
    {
        return strpos($str, $val) === 0;
    }

}

if (!function_exists('sendMessage')) {
    /**
     * Send message as bot
     *
     * @param  int $chatId
     * @param  string $text
     * @return json
     */

    function sendMessage($chatId, $text)
    {
        $query = http_build_query(['chat_id' => $chatId, 'text' => $text]);
        $url = env('BOT_API_URL') . env('BOT_TOKEN') . '/sendMessage?' . $query;
        return telegramRequest($url);
    }

}

if (!function_exists('coindeskRequest')) {
    /**
     * Making request with coindesk api
     *
     * @param  string $url URL
     * @return json
     */
    function coindeskRequest($url)
    {
        $result = json_decode(file_get_contents($url), true);
        if (!$result) {
            throw new RuntimeException('Request failed. Coindesk API reported: '.$result['description']);
        }
        return $result;
    }
}

if (!function_exists('getBtc')) {
    /**
     * Get BTC rate from $currency
     *
     * @param  string $currency
     * @return json
     */

    function getBtc($currency)
    {
        $url = env('COINDESK_API') . '/currentprice/' . $currency .'.json';
        return coindeskRequest($url);
    }

}

if (!function_exists('getCurrencies')) {
    /**
     * Get currencies available from coindesk
     *
     * @return json
     */

    function getCurrencies()
    {
        $url = env('COINDESK_API') . '/supported-currencies.json';
        return coindeskRequest($url);
    }

}
