<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BotConfig extends Model
{
    protected $table = 'bot_config';

    protected $fillable = ['config_key', 'config_value', 'paybee_id'];
}
