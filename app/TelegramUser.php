<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TelegramUser extends Model
{
    protected $fillable = ['paybee_id', 'user_id', 'username', 'code'];
}
