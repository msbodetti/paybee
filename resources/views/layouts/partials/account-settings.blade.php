<div class="card">
    <div class="card-header">{{ __('Account Settings') }}</div>
    {{--@dd($myCurrency)--}}
    <div class="card-body">
        <div class="form-group row mb-0">
            <div class="col-md-12">
                <div class="notice-box">
                    <img src="https://thumbs.gfycat.com/AjarDisguisedAfricanparadiseflycatcher-max-1mb.gif" class="settings-progress" />
                </div>
                <form method="POST" action="{{ route('save-settings') }}" id="save-settings">
                    @csrf
                    <div class="form-group">
                        <label for="currencies">Select your currency</label>
                        <select class="form-control" id="currencies" name="currencies">
                            @foreach($currencies as $currency)
                                @if($currency['currency'] == $myCurrency)
                                    <option value="{{ $currency['currency'] }}" selected>{{ $currency['country'] }} ({{ $currency['country'] }})</option>
                                @else
                                    <option value="{{ $currency['currency'] }}">{{ $currency['country'] }} ({{ $currency['country'] }})</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Save settings</button>
                </form>
                <br/><br>
                <div class="col-md-12">
                    @if(!$telegramUserId)
                        <p>To start chatting with {{ env('BOT_NAME') }}, link your account. After linking, please go to your <a href="/home">Dashboard</a>.</p>
                        <a href="{{ $url }}" class="btn btn-primary" target="popup"  onclick="window.open('{{ $url }}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                            {{ __('Link account') }}
                        </a>
                    @else
                        <p>You have linked your account with {{ env('BOT_NAME') }}, this is your user id: {{ $telegramUserId }}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
