<div class="card">
    <div class="card-header">{{ __('Chat with') }} {{ env('BOT_NAME') }}</div>
    {{--@dd(session()->all())--}}
    <div class="card-body">
        <div class="form-group row mb-0">
            <div class="col-md-12">
                @if(session()->get('botchat'))
                    <div class="chat">
                        <div class="bot-message">
                            <h5><strong>{{ env('BOT_NAME') }}</strong></h5>
                            <p>Hi {{ auth()->user()->name }}, this is the beginning of your chat. <br /><br>
                                Available commands:<br>
                                <strong>/getbtcquivalent</strong> - fetch BTC rate<br />
                                <strong>/getuserid</strong> - get user id
                            </p>
                        </div>
                    </div>
                <img src="https://thumbs.gfycat.com/KindlyActualKawala-size_restricted.gif" class="typing" />
                    <form method="POST" action="{{ route('message') }}" id="messageBot">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-10">
                                    <textarea rows="4" cols="50" name="message" id="message">

                                    </textarea>
                                <input type="hidden" value="{{ auth()->user()->name }}" id="username" />
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Message') }}
                                </button>
                            </div>
                        </div>
                    </form>
                @else
                <p>To start chatting with {{ env('BOT_NAME') }}, link your account. After linking, please refresh this page.</p>
                <a href="{{ $url }}" class="btn btn-primary" target="popup"  onclick="window.open('{{ $url }}','popup','width=600,height=600,scrollbars=no,resizable=no'); return false;">
                    {{ __('Link account') }}
                </a>
                @endif
            </div>
        </div>
    </div>
</div>
