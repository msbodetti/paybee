@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {{--Home--}}
            @if(str_contains(url()->current(), '/home'))
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Hi {{ auth()->user()->name }}, you are logged in!
                    <br /><br />
                    <a href="/bot-config" class="btn btn-primary">
                        {{ __('Account settings') }}
                    </a>

                </div>
            </div>
            <br /><br />
                @include('layouts.partials.telegram-chat')
            @endif
            {{--Account Settings--}}
            @if(str_contains(url()->current(), '/bot-config'))
                @include('layouts.partials.account-settings')
            @endif
        </div>
    </div>
</div>
@endsection
